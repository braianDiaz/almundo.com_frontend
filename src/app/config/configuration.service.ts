import { Injectable } from '@angular/core';
import { BaseConfiguration } from './base';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ConfigService {

    // URLs & Paths
    private appURL: string;
    private allHotelsPath: string;

    constructor() {
      this.setupFromConfigurationFiles();    
    }

    private setupFromConfigurationFiles() {
        let keySet : Set<string> = new Set<string>();
        // Copy base
        for(let propKey in BaseConfiguration) {
          this[propKey] = BaseConfiguration[propKey];
        }
    }

    public getAppUrl(): string{
        return this.appURL;
    }

    public getAllHotelsPath(): string{
        return this.allHotelsPath;
    }

}
