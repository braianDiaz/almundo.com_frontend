import { Pipe } from '@angular/core';

@Pipe({name: 'arrayConvert'})
export class ArrayPipe {
  constructor() {
  }

  public transform(count : number) : number[] {
    return Array(count).fill(1);
  }
}
