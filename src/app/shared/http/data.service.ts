import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Rx';

@Injectable()
export class DataService {
    private options;
    private timeoutOptions = 10000;
    private timeoutErrorCode = "TimeoutError";

    constructor(protected http: Http) {

        let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
        this.options = new RequestOptions({ headers: headers });
    }

    // GET
    protected get(endpoint : string, successCallback, errorCallback)  {
      let service = this;

      this.http.get(endpoint).timeout(this.timeoutOptions)
        .subscribe(
          result => {
            successCallback(service.successHandler(result));
          },
          error => {
            errorCallback(service.errorHandler(error));
          }
        );
    }

    // POST
    protected save(endpoint : string, data : any, successCallback, errorCallback) {
        let service = this;

        return this.http.post(endpoint, JSON.stringify(data), this.options).timeout(this.timeoutOptions)
          .subscribe(
            result => {
              successCallback(service.successHandler(result));
            },
            error => {
              errorCallback(service.errorHandler(error));
            }
          );
    }

    // PUT
    protected update(endpoint : string, data : any, successCallback, errorCallback) {
        let service = this;

        return this.http.put(endpoint, JSON.stringify(data), this.options).timeout(this.timeoutOptions)
          .subscribe(
            result => {
              successCallback(service.successHandler(result));
            },
            error => {
              errorCallback(service.errorHandler(error));
            }
          );
    }

    // DELETE
    protected delete(endpoint: string, successCallback, errorCallback) {
        let service = this;

        return this.http.delete(endpoint).timeout(this.timeoutOptions)
          .subscribe(
            result => {
              successCallback(service.successHandler(result));
            },
            error => {
              errorCallback(service.errorHandler(error));
            }
          );
    }



    public isTimeoutError(error: any): boolean{
      if(typeof error !== "undefined" && error !== null &&
         typeof error.name !== "undefined" && error.name !== null &&
          this.timeoutErrorCode.toLowerCase().indexOf(error.name.toLowerCase()) > -1 ){
            return true;
      }
      return false;
    }

    public isFunctionalError(error: any): boolean{
      if(typeof error !== "undefined" && error !== null &&
         typeof error.errorCode !== "undefined" && error.errorCode !== null &&
          error.errorCode !== "-1" ){
            return true;
      }
      return false;
    }

    public isServerError(error: any): boolean{
      if(typeof error !== "undefined" && error !== null &&
         typeof error.errorCode !== "undefined" && error.errorCode !== null &&
          error.errorCode === "-1" ){
            return true;
      }
      return false;
    }

    public isRequestError(error: any): boolean{
      if(typeof error !== "undefined" && error !== null &&
         typeof error._body !== "undefined" && error._body !== null &&
         typeof error.headers !== "undefined" && error.headers !== null ){

            return true;
      }
      return false;
    }

    private errorHandler(error) : any {
      return error;
    }

    private successHandler(success) : any {
      try {
        return success.json();  
      } catch (error) {
        return {
              errorCode : "-1",
              errorDetail : error.message
          }; 
      } 
    }

}
