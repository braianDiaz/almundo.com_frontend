export class HotelFilter{

    
    name: string;
    starsList : boolean[] = [false,false,false,false,true];
    priceRangeValues: number[] = [0,0];

    constructor(priceRangeValueStart: number , priceRangeValueEnd: number){
        this.priceRangeValues = [priceRangeValueStart,priceRangeValueEnd];
    }


    public setStarsNumber( _pos : number): void{

        if(this.starsList[_pos]){

            if(_pos != 4){
                this.starsList[4] = false;
            }else{
                this.starsList= [false,false,false,false,true];
            }
        }else{
            var _one = false;
            for (var index = 0; index < this.starsList.length; index++) {
                if(this.starsList[index]){
                    _one = true;
                }
            }

            if(!_one){
                this.starsList[4] = true;
            }
        }


        
    }


    public filter(list: any[]) : any[] {
        let result :any[] = [];
        let self = this;

        list.forEach(element => {
            if( !(
                (typeof self.name === "undefined" || self.name === "" || self.name === null )
                || ( element.name.toLocaleLowerCase().indexOf(self.name.toLocaleLowerCase()) > -1 ))){
                return;
            }

            if( !(self.priceRangeValues[0] <= element.price && 
                self.priceRangeValues[1] >= element.price)){
                return;
            }

            if( !(self.starsList[4] || self.starsList[ element.stars - 1])) {
                return
            }
            
            result.push(element);
        });

        return result;
    }
}