import { Component } from '@angular/core';
import { SearchHotelService } from './searchHotelService';
import { HotelFilter } from './searchHotelFilter';

declare var $;

@Component({
  selector: 'app-root',
  templateUrl: './searchHotelView.html',
  styleUrls: ['./searchHotelStyle.css']
})
export class SearchHotelController {

  hotels: any[] = [];
  viewHotels: any[] = [];
  filter: HotelFilter = new HotelFilter(500 , 5000);

  constructor(
    public searchHotelService : SearchHotelService ){
      let self = this;

      searchHotelService.getAll(
        function (result){
          self.hotels = result;
          self.viewHotels = result;
        },

        function (error){
          console.log(error);
        }
        
      );
  }

  public search(): void{
    this.viewHotels = this.filter.filter(this.hotels);
  }
  

  public thereAreResults(): boolean{
    return !(this.viewHotels.length > 0);
  }


  public changeStartSelect( _pos : number): void{
    this.filter.setStarsNumber(_pos);
    this.search();
  }

  public changeRangeValue(e: any): void{
    this.search();
  }

  public showFilter(e: any): void{
    $("#filter-container").addClass("filter-container-over")
  }


  public hiddeFilter(e: any): void{
    $("#filter-container").removeClass("filter-container-over")
  }

}
