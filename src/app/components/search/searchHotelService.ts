import {
    Injectable
  } from '@angular/core';
  import {
    Http
  } from '@angular/http';
  import {
    DataService
  } from '../../shared/http/data.service';
  import {
      ConfigService
  } from '../../config/configuration.service';
  
  @Injectable()
  export class SearchHotelService extends DataService {
  
    constructor(
      protected configService : ConfigService ,
      protected http: Http ) {
  
      super(http);
    }
  
    public getAll(successCallback, errorCallback) {
      let self = this;
      let endpoint: string = 
        this.configService.getAppUrl() +
        this.configService.getAllHotelsPath();
  
      this.get(endpoint, function (result: any) {
        successCallback(result);
      }, function (result: any) {
        errorCallback(result);
      });
    }
  
  }