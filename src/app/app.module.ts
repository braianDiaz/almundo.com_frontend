import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { SearchHotelController } from './components/search/searchHotelController';
import {SliderModule} from 'primeng/primeng';
import { HttpModule } from '@angular/http';
import { Http } from '@angular/http';


//pipes
import { ArrayPipe } from './shared/pipes/array.pipe';

//Services
import {SearchHotelService} from './components/search/searchHotelService';
import {ConfigService} from './config/configuration.service';
import {DataService} from './shared/http/data.service';

@NgModule({
  declarations: [
    ArrayPipe,
    SearchHotelController
  ],
  imports: [
    HttpModule,
    BrowserModule,
    FormsModule,
    SliderModule    
  ],
  providers: [
    DataService,
    ConfigService,
    SearchHotelService
  ],
  bootstrap: [SearchHotelController]
})
export class AppModule { }
